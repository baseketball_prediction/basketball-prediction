# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.
from bs4 import BeautifulSoup, Comment
import os
import urllib.request
import socket
class dataScrap():
    def playOffsTotalScrap(self, url,file,teamName,year):              
        try:
            page=urllib.request.urlopen(url)
        except:         
            print("Error in internet connection")
        soup = BeautifulSoup(page,"html.parser")
        div=soup.find('div', id='all_playoffs_totals')
        comments = div.findAll(text=lambda text:isinstance(text, Comment))
        var1 = ""
        #enhanced forloop
        for comment in comments:
           var1 = var1+comment
        soup1=BeautifulSoup(var1,"html.parser")
        table=soup1.find('table', id='playoffs_totals')
        table_Head=table.thead
        tableBody=table.find('tbody')
        rowsData=""
        for rows in tableBody.findAll('tr'):
            cellData = teamName+","+year            
            for cells in rows.findAll('td'):
                cellData=cellData+","+cells.text
            rowsData=rowsData+"\n"+cellData
        print(rowsData)
       
        file.write(bytes(rowsData, encoding="ascii", errors="ignore"))

#Store the output to excel file.
file=open(os.path.expanduser("PlaysoffTotalData8.csv"),"wb")
#Header of the table
tabel_header_main = "TeamName,Year,Player,Age,G,GS,MP,FG,FGA,FG%,3P,3PA,3P%,2P,2PA,2P%,eFG%,FT,FTA,FT%,ORB,DRB,TRB,AST,STL,BLK,TOV,PF,PTS"
file.write(bytes(tabel_header_main,encoding="ascii", errors="ignore"))

#Start collecting data from website
scrapThePage = dataScrap()


#URL for main website
url = "http://www.basketball-reference.com/playoffs/"
page=urllib.request.urlopen(url)
soup = BeautifulSoup(page,"html.parser")
table=soup.find('table', id='champions_index')
storeYears = []
tableBody=table.find('tbody')
for rows in tableBody.findAll('tr'):
    cellData = ""
    for year in rows.findAll('th'):
        #Store all the years available
        storeYears.append(year.text)
        
#Iterate available years in web page
for year in storeYears:    
    yearWiseUrl = "http://www.basketball-reference.com/playoffs/NBA_"+year+".html"              
    page=urllib.request.urlopen(yearWiseUrl)
    try:
        page=urllib.request.urlopen(yearWiseUrl)
    except:
        print("Error in internet connection")
    soup = BeautifulSoup(page,"html.parser")
    table=soup.find('table', id='all_team_stats')    
    tableBody=table.find('tbody')
    for rows in tableBody.findAll('tr'):
        cellData = ""
        teamName = rows.find('td').text                
        for row in rows.findAll('td'):            
            links = row.findAll('a')
            for link in links:
                #Get the URL for collecting Playoffs data
                #print(link.get('href'))
                newLink = "http://www.basketball-reference.com" + link.get('href')
                scrapThePage.playOffsTotalScrap(newLink,file,teamName,year)


__author__ = "S524960"
__date__ = "$Oct 12, 2016 8:22:57 AM$"

if __name__ == "__main__":
    print("Playoffs Total Data")
